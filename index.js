const calculateHash = str => {
    let hash = str
        .split("") // Separator is an empty string (""), str is converted to an array of each of its UTF-16 "characters".
        .map((c, i) => str.charCodeAt(i)) // Converts letters to UTF-16 character
        .map(c => c + 2) // Adds 2 to every number of the array
        .map(c => String.fromCharCode(c)) // Convert the UTF-16 values to characters
        .join(""); // Separator is an empty string, all elements are joined without any characters in between them
    return Buffer.from(hash).toString("base64");
};

console.log(calculateHash("axf d"));
// Hash du mot à trouver ZWpxZQ==