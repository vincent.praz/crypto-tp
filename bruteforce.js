// Fonction de la donnée
const calculateHash = str => {
    let hash = str
        .split("") // Separator is an empty string (""), str is converted to an array of each of its UTF-16 "characters".
        .map((c, i) => str.charCodeAt(i)) // Converts letters to UTF-16 character
        .map(c => c + 2) // Adds 2 to every number of the array
        .map(c => String.fromCharCode(c)) // Convert the UTF-16 values to characters
        .join(""); // Separator is an empty string, all elements are joined without any characters in between them
    return Buffer.from(hash).toString("base64");
};
// Hash du mot à trouver ZWpxZQ==


// Fonction inverse
const invertedFunction = hash => {
    hash = Buffer.from(hash, 'base64').toString();
    let str = hash
        .split("")
        .map((c, i) => hash.charCodeAt(i))
        .map(c => c - 2)
        .map(c => String.fromCharCode(c))
        .join("");
    return str;
};


// Fonction "bruteforce" sur le décalage de charactère
const brute = hash => {
    hash = Buffer.from(hash, 'base64').toString();
    let array = [];
    for (let index = 0; index < 1112064; index++) {
        let str = hash
            .split("")
            .map((c, i) => hash.charCodeAt(i))
            .map(c => c - index)
            .map(c => String.fromCharCode(c))
            .join("");
        str = (index + 1) + " : " + str
        array.push(str);
    }
    return (array);
};


// Fonction en mode "boite noire"
const blackBox = hash =>{
    // Variable "dictionnaire"
    let dict = [' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

    // array utilisé pour créer les mots pour le bruteforce
    let array = [];

    // boucles testant toutes les possibilités
    for (let c0 = 0; c0 < 27; c0++) {
        array[0] = dict[c0];
        for (let c1 = 0; c1 < 27; c1++) {
            array[1] = dict[c1];
            for (let c2 = 0; c2 < 27; c2++) {
                array[2] = dict[c2];
                for (let c3 = 0; c3 < 27; c3++) {
                    array[3] = dict[c3];

                    //Build du mot en une string
                    let str = array[0] + array[1] + array[2] + array[3];

                    let result = calculateHash(str);

                    // On compare le hash passé en param au hash de notre string buildée
                    if(hash === result){
                        return "Le mot original est " + str;
                    }
                }
            }
        }
    }
}



// console.log(invertedFunction("ZWpxZQ=="));

// console.log(brute("ZWpxZQ=="));

console.log(blackBox("ZWpxZQ=="))
